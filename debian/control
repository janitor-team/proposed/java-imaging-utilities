Source: java-imaging-utilities
Section: java
Priority: optional
Maintainer: Stuart Prescott <stuart@debian.org>
Build-Depends:
 debhelper-compat (= 12)
Build-Depends-Indep:
 ant,
 default-jdk,
 default-jdk-doc,
 texlive-base-bin,
 texlive-latex-extra,
 texlive-latex-recommended
Standards-Version: 4.4.1
Homepage: https://sourceforge.net/projects/jiu/
Vcs-Git: https://salsa.debian.org/debian/java-imaging-utilities.git
Vcs-Browser: https://salsa.debian.org/debian/java-imaging-utilities

Package: libjiu-java
Architecture: all
Depends:
 ${misc:Depends}
Suggests:
 libjiu-java-doc
Description: library to load, analyze, process and save pixel images
 JIU, the Java Imaging Utilities, is a library which offers functionality
 to load, analyze, process and save pixel images.
 .
 It can handle a variety of different image formats (PBM, PNG, GIF, TIFF,
 PSD etc) and perform a number of sophisticated transformations to the
 images including color adjustments, analysis and image filtering.

Package: libjiu-java-doc
Architecture: all
Section: doc
Depends:
 default-jdk-doc,
 ${misc:Depends}
Description: API documentation for jiu java library for pixel images
 JIU, the Java Imaging Utilities, is a library which offers functionality
 to load, analyze, process and save pixel images.
 .
 This package contains the API documentation for the library.
 .
 See the package libjiu-java for further information.
