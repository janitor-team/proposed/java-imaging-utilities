java-imaging-utilities (0.14.3-5) UNRELEASED; urgency=medium

  * Update standards version to 4.4.1, no changes needed.
  * Bump debhelper from old 11 to 12.
  * Fix day-of-week for changelog entries 0.14.2-1.

 -- Debian Janitor <janitor@jelmer.uk>  Thu, 05 Dec 2019 19:04:20 +0000

java-imaging-utilities (0.14.3-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/watch: Use https protocol

  [ Stuart Prescott ]
  * Use debhelper-compat (= 11).
  * Update Standards-Version to 4.3.0 (no changes required).

 -- Stuart Prescott <stuart@debian.org>  Sat, 26 Jan 2019 18:47:00 +1100

java-imaging-utilities (0.14.3-3) unstable; urgency=medium

  * Fix classes to build with OpenJDK rather than gcj (Closes: #894524).
  * Remove unneeded dpkg-parsechangelog calls from d/rules.
  * Update Standards-Version to 4.1.3 (no changes required).
  * Move to debhelper compat 11.
  * Add Vcs-* headers pointing to salsa.debian.org.

 -- Stuart Prescott <stuart@debian.org>  Fri, 06 Apr 2018 21:29:40 +1000

java-imaging-utilities (0.14.3-2) unstable; urgency=medium

  * Force latex to use SOURCE_DATE_EPOCH to make manual reproducible.

 -- Stuart Prescott <stuart@debian.org>  Mon, 08 Aug 2016 15:04:08 +1000

java-imaging-utilities (0.14.3-1) unstable; urgency=medium

  * New upstream version.
    - drop repackaging that is no longer required.
    - update copyright information.
    - add new homepage.
  * Update maintainer information.
  * Update Standards-Version to 3.9.8 (no changes required).
  * Set locale in build to make API documentation reproducible.
  * Switch to debhelper compat 9.
  * Use openjdk/default-jdk to build documentation but use gcj-compat/gcc to
    build source (weird but the only combination that currently works).
  * Stop having the documentation package depend on the library package.
  * Add simple DEP-8/autopkgtest test to package.

 -- Stuart Prescott <stuart@debian.org>  Sun, 07 Aug 2016 14:57:20 +1000

java-imaging-utilities (0.14.2+3-4) unstable; urgency=low

  * Add build-depends on texlive-latex-extra (Closes: #669532).
  * Bump standards to 3.9.3 (no changes).
  * Update copyright file format.

 -- Stuart Prescott <stuart+debian@nanonanonano.net>  Thu, 19 Apr 2012 22:59:31 +0100

java-imaging-utilities (0.14.2+3-3) unstable; urgency=low

  * Fix typo in description (Closes: #609248).
  * Extract upstream package version correctly for jar link (Closes: #612295).
  * Split doc-base entry as HTML and PDF data were not just different formats
    for the same information.
  * Add ${misc:Depends}.
  * Switch to dpkg-source 3.0 (quilt) format.
  * Switch to debhelper short forms for debian/rules.
  * Disable watch file as upstream website has disappeared.
  * Bump standards to 3.9.2 (no changes).
  * Set DM-Upload-Allowed: yes

 -- Stuart Prescott <stuart+debian@nanonanonano.net>  Tue, 31 May 2011 08:48:39 +0100

java-imaging-utilities (0.14.2+3-2) unstable; urgency=low

  * Bump standards to 3.8.3 (no changes).
  * Fix bashism in debian/rules (Closes: #535384).

 -- Stuart Prescott <stuart+debian@nanonanonano.net>  Tue, 15 Sep 2009 15:35:24 +0100

java-imaging-utilities (0.14.2+3-1) unstable; urgency=low

  * new source package from updated repackaging script.
  * copyright: update years, add note about Resample.java.
  * repackaging: new version of repackager maintains consistent md5 for tarball
  * get-orig-source and watch file now linked and repackage source.
  * remove tetex packages as options for build-deps now that lenny is released
    (remove lintian override for this).
  * bump standards version to 3.8.1 (no changes required)
  * move build.xml into debian directory
  * make sure clean target cleans up correctly

 -- Stuart Prescott <stuart+debian@nanonanonano.net>  Mon, 20 Apr 2009 01:14:05 +0100

java-imaging-utilities (0.14.2+2-1) unstable; urgency=low

  * Remove PDF from source package.
  * Add (pdf)latex tools to build-depends.
  * Add lintian source override so that it doesn't complain about the optional
    build-dep on the obsolete tetex packages.
  * Rebuild PDF from latex source from debian/rules.
  * Don't include .tex version of manual as well as PDF version in binary
    package.
  * Remove unneeded "jiu" script from debian diff.
  * Move "jiuawt" into the debian directory.
  * Convert debian/copyright into machine-readable form.

 -- Stuart Prescott <stuart+debian@nanonanonano.net>  Mon, 21 Jul 2008 15:26:04 +0100

java-imaging-utilities (0.14.2+1-3) unstable; urgency=low

  * Set JAVA_HOME in debian/rules only if it is not already set in the
    environment.
  * Remove unneeded targets from debian/rules

 -- Stuart Prescott <stuart+debian@nanonanonano.net>  Sat, 19 Jul 2008 00:37:33 +0100

java-imaging-utilities (0.14.2+1-2) unstable; urgency=low

  * Explicitly set JAVA_HOME in debian/rules to prevent FTBFS when there
    is an installed JRE that is not compatible with the JDK from the
    Build-Depends.
  * Fix years in debian/copyright.
  * Include statement from upstream's README in debian/copyright to the
    effect that it is released under the GPLv2.
  * Remove java-imaging-utilities binary package and move example script
    into libjiu-java-doc package.

 -- Stuart Prescott <stuart+debian@nanonanonano.net>  Sat, 19 Jul 2008 00:03:35 +0100

java-imaging-utilities (0.14.2+1-1) unstable; urgency=low

  * Add Debian revision to the upstream tarball version
  * Strip out autogenerated documentation from the upstream tarball
  * Fix GPL link in copyright (explicitly GPL-2 not just GPL)
  * Change API doc location as per upcoming java policy
  * Automatically version the jar using the changelog version

 -- Stuart Prescott <stuart+debian@nanonanonano.net>  Tue, 01 Jul 2008 00:27:48 +0100

java-imaging-utilities (0.14.2-2) unstable; urgency=low

  * Bump standards version to 3.8.0 (no changes required)
  * Rename doc package to libjiu-java-doc

 -- Stuart Prescott <stuart+debian@nanonanonano.net>  Mon, 23 Jun 2008 23:11:19 +0100

java-imaging-utilities (0.14.2-1) unstable; urgency=low

  * Initial release (Closes: #431907)
  * New upstream release

 -- Stuart Prescott <stuart+debian@nanonanonano.net>  Sat, 07 Jul 2007 00:17:36 +0100

java-imaging-utilities (0.14.1-1) unstable; urgency=low

  * Initial packaging efforts.

 -- Stuart Prescott <stuart+debian@nanonanonano.net>  Wed, 11 Apr 2007 18:14:04 +0100
