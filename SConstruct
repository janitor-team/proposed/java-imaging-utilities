# SCons build rules for JIU

import os

env = Environment()

# Note that setting the source version does not provide backwards compatibility,
# that would require -target, -bootclasspath and -extdir parameters.
java_source_version = '1.3'
env['JAVAVERSION'] = java_source_version
env['JAVACFLAGS'] = '-source ' + java_source_version

# Auto-detect java based on JAVA_HOME, if set
if os.environ.has_key('JAVA_HOME'):
    java_home = os.environ['JAVA_HOME']
    env['ENV']['PATH'] = os.path.join(java_home, 'bin')

# Surprisingly, the Eclipse compiler seems to produce significantly faster
# code than a standard jdk6 javac, so try to use it if possible.
if os.environ.has_key('ECLIPSE_HOME'):
    # try to use eclipse java compiler
    java_env = env.Clone(tools=['eclipse_javac'])
    java_env['JAVACFLAGS'] = '-1.3 -warn:-serial -warn:-unusedPrivate'
else:
    java_env = env

# Build classes
java_env.Java('classes', '.')

# Install resources
env.Install('classes', ['resources'])

# Build jar
# Note: the Jar builder autodetects the manifest as such, as long as it's
# explicitly listed as a source.
jiu_jar = env.Jar('jiu.jar', ['classes', 'META-INF/MANIFEST.MF'], JARCHDIR='classes')
#env.Default(jiu_jar)

# Javadoc
apidoc = env.Command('doc/api', 'net', 'javadoc @packages @docs-html-options')
# AlwaysBuild tells scons to ignore dependencies, not really to always build it
AlwaysBuild(apidoc)
env.Alias('javadoc', apidoc)

# Bundling
def versioned_mercurial_files():
    '''Generates a list of versioned files (in mercurial).'''
    import subprocess
    cmd = 'hg status -cman'.split()
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    return [line.strip() for line in p.stdout]

versioned_files = versioned_mercurial_files()

if versioned_files:
    VERSION = '0.14.3'
    BUNDLENAME = 'java-imaging-utilities-' + VERSION
    SUFFIXES = ['.zip', '.tar.gz', '.tar.bz2']
    
    pkg_env = Environment(tools=['default', 'bundle_tool'])
    pkg_env['BUNDLE_PREFIX'] = 'jiu-' + VERSION + '/'
    
    pkg_env.Bundle([BUNDLENAME + '-core' + suffix for suffix in SUFFIXES], versioned_files)
    pkg_env.Bundle([BUNDLENAME + suffix for suffix in SUFFIXES], versioned_files + ['jiu.jar', Dir('doc/api')])
else:
    print 'Failed to get a list of versioned (Mercurial) files, bundling skipped.'
